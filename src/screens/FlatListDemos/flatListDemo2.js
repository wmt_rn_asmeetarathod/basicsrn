/* eslint-disable */
import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, StyleSheet, FlatList, Image, SafeAreaView } from 'react-native';

const FlatListDemo2 = () => {
    const [data, setData] = useState([]);
    const [refreshing, setRefreshing] = useState(true);

    const fetchCats = () => {
        setRefreshing(true);
        fetch('https://api.thecatapi.com/v1/images/search?limit=10&page=1')
            .then(res => {
                //console.log("res ==>> ",res.json());
                return res.json();
            })
            .then(resJson => {
                console.log("Json Response ==>> ", resJson);
                setData(resJson);
                setRefreshing(false);
                
            })
            .catch(e => console.log("Error ", e));

        // console.log("data ==>> ", data);
    }



    useEffect(() => {
        fetchCats();
    }, []);

    const RenderItemCompo = ({ itemData }) => {
        return (
            <View>
                <TouchableOpacity style={styles.container}>
                    <Image style={styles.image} source={{ uri: itemData.url }} />
                </TouchableOpacity>
            </View>
        );
    }

    const ItemSeperator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "rgba(0,0,0,0.5)",
                }}
            />
        );
    }

    return (
        
            <SafeAreaView style={{flex:1}}>
                <FlatList
                    data={data}
                    renderItem={({ item }) => (<RenderItemCompo itemData={item} />)}
                    keyExtractor={item => item.id.toString()}
                    ItemSeparatorComponent={ItemSeperator}
                    refreshing={refreshing}
                    onRefresh={() => setRefreshing(false)}
                />
            </SafeAreaView>
        
    );
}

const styles = StyleSheet.create({
    container: {
        
        height: 300,
        margin: 10,
        backgroundColor: '#FFF',
        borderRadius: 6,
  
    },
    image: {
        height: '100%',
        borderRadius: 4,
    },
});

export default FlatListDemo2;