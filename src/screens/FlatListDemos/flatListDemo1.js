/* eslint-disable */
import React from 'react';
import { Text,View,FlatList,StyleSheet } from 'react-native';

const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
    },
  ];

const Item= ({title}) =>{
    return(
        <View>
            <Text>{title}</Text>
        </View>
    )
}

  
const FlatListDemo1 = () =>{
    // const renderItem=({item})=> (<Item title={item.title} />);
    return(
        <FlatList data={DATA} horizontal={true} renderItem={({item})=>(<Item title={item.title} />)} keyExtractor={item=> item.id}></FlatList>
    );
}


const styles=StyleSheet.create({

});

export default FlatListDemo1;