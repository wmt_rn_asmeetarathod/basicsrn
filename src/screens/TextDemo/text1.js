/* eslint-disable */
import React, { useState } from 'react';
import { Text,View,StyleSheet } from 'react-native';

const onPressTitle = () =>{
    console.log("Title Pressed");
};

const TextInNest = () =>{
    const titleText=useState("Language");
    const bodyText=useState("React Native");
    return(
        <Text style={styles.baseText} >
            <Text style={styles.titleText} onPress={onPressTitle} >
                {titleText}
                {"\n"}
                {"\n"}
                </Text>
            <Text numberOfLines={3}>{bodyText}</Text>    
            
        </Text>
    );

};


const styles=StyleSheet.create({
    baseText:{
        fontFamily: "Cochin",
        backgroundColor: "red"
        
    },
    titleText : {
        fontSize :20,
        fontWeight: "bold"
    }
});

export default TextInNest;