/* eslint-disable */
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const BoldAndBeautiful = () => {
    return (
        <Text style={ styles.baseText}>
            Bold Text
            <Text style ={ styles.innerText} ellipsizeMode="middle" numberOfLines={1}>Red Text The line is displayed so that the end fits in the container 
            and the missing text at the beginning of the line is indicated by an ellipsis glyp.</Text>
        </Text>
    )
}

const styles = StyleSheet.create({
    baseText: {
        fontWeight: 'bold'
    },
    innerText: {
        color: 'red'
    }
});

export default BoldAndBeautiful;