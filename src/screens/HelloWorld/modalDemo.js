/* eslint-disable */
import React, { useState } from 'react';
import { Modal,StyleSheet,View,Text,Pressable,Alert } from 'react-native';

const ModalDemo = () =>{
    const [modalVisible,setModalVisible]=useState(false);
    return(
        <View style={styles.centeredView}>
            <Modal animationType='slide' transparent={true} visible={modalVisible} onRequestClose={()=>{
                Alert.alert("Modal has been Closed!");
                setModalVisible(!modalVisible);
            }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>Hello</Text>
                        <Pressable onPress={()=>setModalVisible(!modalVisible)} style={styles.btnShow} android_ripple={{color:'pink',borderless:false}}>
                        <Text style={styles.modalText} >Hide Modal</Text>
                        </Pressable>
                        
                    </View>
                </View>
            </Modal>
            <Pressable onPress={()=> setModalVisible(true)} 
            style={styles.btnShow} 
            android_ripple={{color:'red',borderless:false}} 
            hitSlop={{top:20,bottom:20,left:50,right:50}}>
                <Text style={styles.modalText}>Show Modal</Text>
            </Pressable>
        </View>
    );
}

const styles=StyleSheet.create({
 centeredView:{
     flex:1,
     justifyContent:'center',
     alignItems:'center',
     marginTop:22
 },
 btnShow:{
    borderRadius: 20,
    padding:10,
    elevation:2,
    backgroundColor:'purple',
    
 },
 modalText:{
     padding:10,
     color:'white',
     fontSize:18
 },
 modalView:{
     margin:20,
     backgroundColor:'white',
     borderRadius: 20,
     padding:35,
     alignItems: 'center',
     shadowColor:'#000',
     shadowOpacity:0.25,
     shadowRadius:4,
     elevation:5
 }
});

export default ModalDemo;
