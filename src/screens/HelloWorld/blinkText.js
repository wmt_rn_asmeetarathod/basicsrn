/* eslint-disable */

import React, {useEffect, useState} from 'react';
import {Text, View,StyleSheet} from 'react-native';

const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems : 'center'
    }
});

const Blink = (props) => {
  const [isShowingText, setIsShowingText] = useState(true);

  useEffect(() => {
    const toggle = setInterval(() => {
      setIsShowingText(!isShowingText);
    }, 1000);
    return ()=> clearInterval(toggle);
  })
  if(!isShowingText){
      return null;
  }
  return <Text>{props.text}</Text>
};

const BlinkApp = () =>{
   return(
    <View style={styles.container}>
    <Blink text="Blinking Text" />
    <Blink text="Holaa!!" />
    <Blink text="....Sayonara...." />
</View>
   )
}

export default BlinkApp;