/* eslint-disable */
import React from 'react';
import { View,KeyboardAvoidingView,TextInput,StyleSheet,Text,Platform
    ,TouchableWithoutFeedback,Button,Keyboard } from 'react-native';

const KeyBoardAvoidDemo = () =>{
    return(
        <KeyboardAvoidingView keyboardVerticalOffset={100}  behavior={Platform.OS === "ios" ? "padding" : "height"} style={styles.container}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View>
                    <Text>Header</Text>
                    <TextInput placeholder="Username" />
                    <Button title="Submit" onPress={()=>null}></Button>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    );
}

const styles=StyleSheet.create({
    container:{
        flex:1
    }
});

export default KeyBoardAvoidDemo;