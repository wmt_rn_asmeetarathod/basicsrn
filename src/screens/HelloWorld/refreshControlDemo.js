/* eslint-disable */
import React, { useCallback, useState } from 'react';
import { RefreshControl,StyleSheet,Text,SafeAreaView,ScrollView } from 'react-native';

const wait=(timeout)=>{
    return new Promise(resolve=>setTimeout(resolve,timeout));
}

const RefreshControlDemo = () =>{
    const [refreshing,setRefreshing]=useState(false);
    const onRefresh= useCallback(()=>{
        setRefreshing(true);
        wait(2000).then(()=>setRefreshing(false));
    },[]);
    return(
        <SafeAreaView>
            <ScrollView 
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} colors={['white']} progressBackgroundColor='purple' />}>
                <Text>Pull Down to Refresh</Text>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles=StyleSheet.create({

});

export default RefreshControlDemo;