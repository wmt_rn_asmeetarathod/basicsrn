/* eslint-disable */
import React, { useState } from 'react';
import { Switch , View , Text } from 'react-native';

const Switch1 = () =>{
    const [switchValue,setSwitchValue] = useState(false);
    return(
        <View>
            <Text>Switch Example</Text>
            <Text>{switchValue ? 'on' : 'off'}</Text>
            <Switch
            value={switchValue}
            onValueChange={(switchValue)=>setSwitchValue(switchValue)}
            thumbColor='red'
            trackColor={{false: 'purple',true: 'green'}}
            />
        </View>
    );
}

export default Switch1;