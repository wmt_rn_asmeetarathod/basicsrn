/* eslint-disable */
import React from 'react';
import { Text,StyleSheet,View, Button, Alert } from 'react-native';

const BtnDemo1 = () =>{
    return(
        <Button title="Click!" onPress={()=> Alert.alert('Button Clicked!')} accessibilityLabel="Button"></Button>
    )
}

export default BtnDemo1;