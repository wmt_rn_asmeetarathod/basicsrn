/* eslint-disable */
import React from 'react';
import { StatusBar,View } from 'react-native';

const StatusBar1 = () =>{
    return(
        <View>
            <StatusBar 
            backgroundColor="purple"
            barStyle="light-content"
            hidden={false}
            translucent={true}
            />
        </View>
    );
}

export default StatusBar1;