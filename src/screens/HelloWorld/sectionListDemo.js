/* eslint-disable */
import React from 'react';
import { StyleSheet, Text, SafeAreaView, View, FlatList, SectionList, StatusBar } from 'react-native';

const DATA = [
    {
        title: "Main dishes",
        data: ["Pizza", "Burger", "Risotto"]
    },
    {
        title: "Sides",
        data: ["French Fries", "Onion Rings", "Fried Shrimps"]
    },
    {
        title: "Drinks",
        data: ["Water", "Coke", "Beer"]
    },
    {
        title: "Desserts",
        data: ["Cheese Cake", "Ice Cream"]
    }
];

const Item = ({ title }) => {
    return (
        <View style={styles.item}>
            <Text style={styles.itemText}>{title}</Text>
        </View>
    );
}

const HeaderCompo = ()=>{
    return(
        <View>
            <Text>HeaderCompo</Text>
        </View>
    );
}
const FooterCompo = ()=>{
    return(
        <View>
            <Text>FooterCompo</Text>
        </View>
    );
}
const SectionHeader=()=>{
    return(
        <View>
            <Text>SectionHeader</Text>
        </View>
    );
}
const SectionFooter=()=>{
    return(
        <View>
            <Text>SectionFooter</Text>
        </View>
    );
}
const SectionListDemo = () => {
    return (
        <SafeAreaView style={styles.container}>
            <SectionList
                sections={DATA}
                ListHeaderComponent={HeaderCompo}
                ListFooterComponent={FooterCompo}
                renderSectionHeader={SectionHeader}
                renderSectionFooter={SectionFooter}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item }) => <Item title={item} />}
                renderSectionHeader={({ section: { title } }) => (
                    <Text>{title}</Text>
                )}>

            </SectionList>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        marginHorizontal: 16
    },
    item: {
        margin: 7,
        backgroundColor: 'purple',

    },
    itemText: {
        color: 'white',
        fontSize: 20,
        padding: 7
    }
});

export default SectionListDemo;