/* eslint-disable */

import React from 'react';
import { Text, StyleSheet, ActivityIndicator, View } from 'react-native';

const Indicator = () => {
    return (
        <View style={[styles.conatainer, styles.horizontal]}>
            <ActivityIndicator  color="red"/>
            <ActivityIndicator size='large' color="#0000ff" />
            <ActivityIndicator size='large' color="#00ff00" />
        </View>
    );
}

const styles = StyleSheet.create({
    conatainer: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});


export default Indicator;