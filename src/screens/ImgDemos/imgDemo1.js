/* eslint-disable */
import React from 'react';
import { Image , StyleSheet, View } from 'react-native';

const ImgDemo1 = () =>{
    const imgUrl='https://reactnative.dev/img/tiny_logo.png';
    return(
        <View style={ styles.container }>
            <Image 
            style={styles.tinyLogo}
            source={require('../../assets/a5.jpeg')}
            fadeDuration= {1000}
            />
            
            <Image
            style={styles.tinyLogo}
            source={{uri : imgUrl}}
            blurRadius= {1}
            
            />
            <Image
            style={styles.logo}
            source={{uri:  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg=='}}
            />

        </View>
    );
}

const styles=StyleSheet.create({
    container:{
        paddingTop:50,
    },
    tinyLogo:{
        width:50,
        height:50
    },
    logo:{
        width:70,
        height:70
    }
});
export default ImgDemo1;