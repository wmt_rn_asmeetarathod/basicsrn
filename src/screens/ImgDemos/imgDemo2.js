/* eslint-disable */

import React from 'react';
import { ImageBackground,View,Text,StyleSheet } from 'react-native';


const imgUrl={uri: "https://reactjs.org/logo-og.png"};
const ImgDemo2 = () =>{
 return(
    <View style={styles.container}>
    <ImageBackground source={imgUrl} style={styles.image}>
        <Text style={styles.text}>Inside</Text>
    </ImageBackground>
</View>
 )
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'column'
    },
    image:{
        flex:1,
        resizeMode:'cover',
        justifyContent:'center'
    },
    text:{
        opacity: 0.4,
        color:'white',
        fontSize: 42,
        fontWeight: 'bold',
        textAlign: 'center',
        backgroundColor: 'red'
    }

});
export default ImgDemo2;