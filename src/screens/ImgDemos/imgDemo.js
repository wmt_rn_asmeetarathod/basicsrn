/* eslint-disable */
import React from 'react';
import {Image} from 'react-native';

const ImgDemo = () => {
  let pic = {  uri: 'https://cdn.auth0.com/blog/illustrations/react.png',  };
  return (
    <Image source={pic} style={{width: 193, height: 110, marginTop: 50}} />
  );
};

export default ImgDemo;