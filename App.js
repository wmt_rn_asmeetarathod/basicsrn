/* eslint-disable */
import React from 'react';

import HelloWorldApp from './helloWorld';
import LotsOfGreetings from './src/screens/HelloWorld/helloProps';
import Counter from './src/screens/HelloWorld/counter';
import BlinkApp from './src/screens/HelloWorld/blinkText';
import Indicator from './src/screens/HelloWorld/activityIndicatorDemo';
import BtnDemo1 from './src/screens/HelloWorld/btnDemo1';
import KeyBoardAvoidDemo from './src/screens/HelloWorld/keyboardAvoidDemo';
import ModalDemo from './src/screens/HelloWorld/modalDemo';
import RefreshControlDemo from './src/screens/HelloWorld/refreshControlDemo';
import SectionListDemo from './src/screens/HelloWorld/sectionListDemo';
import StatusBar1 from './src/screens/HelloWorld/statusBar1';
import Switch1 from './src/screens/HelloWorld/switch1';



import FlatListDemo1 from './src/screens/FlatListDemos/flatListDemo1';
import FlatListDemo2 from './src/screens/FlatListDemos/flatListDemo2';

import TextInNest from './src/screens/TextDemo/text1';
import BoldAndBeautiful from './src/screens/TextDemo/text2';

import ImgDemo from './src/screens/ImgDemos/imgDemo';
import ImgDemo1 from './src/screens/ImgDemos/imgDemo1';
import ImgDemo2 from './src/screens/ImgDemos/imgDemo2';

const App: () => React$Node = () => {
  return (
    <>
      {/* __FlatListDemos__ */}

      {/* <FlatListDemo2 /> */}
      {/* <FlatListDemo1 /> */}

      {/* __FlatListDemos__ */}


      {/* __HelloWorld__ */}
      {/*       
      <BtnDemo1 />
      <Indicator />
      
      <BlinkApp />
      <ImgDemo />
      <Counter />
      <LotsOfGreetings />
      <HelloWorldApp /> */}
      {/* <ImgDemo1 /> */}
      {/* <ImgDemo2  /> */}
      {/* <KeyBoardAvoidDemo /> */}
      {/* <ModalDemo /> */}
      {/* <RefreshControlDemo /> */}
      {/* <SectionListDemo /> */}
      {/* <StatusBar1 /> */}
      {/* <Switch1 /> */}



      {/* __HelloWorld__ */}


      {/* __TextDemo__ */}

      {/* <TextInNest />
      <BoldAndBeautiful  />  */}

      {/* __TextDemo__ */}

    </>
  );
};

export default App;
